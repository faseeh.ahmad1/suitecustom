<?php
// created: 2022-08-10 12:44:55
$GLOBALS['tabStructure'] = array (
  'LBL_TABGROUP_SALES' => 
  array (
    'label' => 'LBL_TABGROUP_SALES',
    'modules' => 
    array (
      0 => 'reg_Teacher',
      1 => 'reg_Section',
      2 => 'reg_Course',
      3 => 'Contacts',
    ),
  ),
  'LBL_TABGROUP_MARKETING' => 
  array (
    'label' => 'LBL_TABGROUP_MARKETING',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Contacts',
      3 => 'Leads',
      4 => 'Campaigns',
      5 => 'Prospects',
      6 => 'ProspectLists',
    ),
  ),
  'LBL_TABGROUP_SUPPORT' => 
  array (
    'label' => 'LBL_TABGROUP_SUPPORT',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Cases',
      3 => 'Bugs',
    ),
  ),
  'LBL_TABGROUP_ACTIVITIES' => 
  array (
    'label' => 'LBL_TABGROUP_ACTIVITIES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Calendar',
      2 => 'Calls',
      3 => 'Meetings',
      4 => 'Emails',
      5 => 'Tasks',
      6 => 'Notes',
    ),
  ),
  'LBL_TABGROUP_COLLABORATION' => 
  array (
    'label' => 'LBL_TABGROUP_COLLABORATION',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Emails',
      2 => 'Documents',
      3 => 'Project',
    ),
  ),
);