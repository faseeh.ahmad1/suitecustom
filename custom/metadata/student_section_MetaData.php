<?php
$dictionary["section_student"] = array(
    'true_relationship_type' => 'many-to-many',
    'from_studio' => false,
    'relationships' =>
    array(
        'section_student' =>
        array(
            'lhs_module' => 'reg_Section',
            'lhs_table' => 'reg_section',
            'lhs_key' => 'id',
            'rhs_module' => 'Contacts',
            'rhs_table' => 'contacts',
            'rhs_key' => 'id',
            'relationship_type' => 'many-to-many',
            'join_table' => 'section_student',
            'join_key_lhs' => 'section_id',
            'join_key_rhs' => 'student_id',
        ),
    ),
    'table' => 'section_student',
    'fields' =>
    array(
        0 =>
        array(
            'name' => 'id',
            'type' => 'varchar',
            'len' => 36,
        ),
        1 =>
        array(
            'name' => 'date_modified',
            'type' => 'datetime',
        ),
        2 =>
        array(
            'name' => 'deleted',
            'type' => 'bool',
            'len' => '1',
            'default' => '0',
            'required' => true,
        ),
        3 =>
        array(
            'name' => 'student_id',
            'type' => 'varchar',
            'len' => 36,
        ),
        4 =>
        array(
            'name' => 'section_id',
            'type' => 'varchar',
            'len' => 36,
        ),
    ),
    'indices' =>
    array(
        0 =>
        array(
            'name' => 'section_student_pk',
            'type' => 'primary',
            'fields' =>
            array(
                0 => 'id',
            ),
        ),
        1 =>
        array(
            'name' => 'section_student_alt',
            'type' => 'alternate_key',
            'fields' =>
            array(
                0 => 'section_id',
                1 => 'student_id',
            ),
        ),
    ),
);
