<?php
// created: 2022-08-02 17:33:12
$mod_strings['LBL_REPORTS_TO'] = 'Reports To:';
$mod_strings['LBL_CONTACTS'] = 'Students';
$mod_strings['LBL_LEADS'] = 'Leads';
$mod_strings['LBL_CONTACT_ID'] = 'Student ID';
$mod_strings['LBL_CONVERTED_CONTACT'] = 'Converted Student:';
