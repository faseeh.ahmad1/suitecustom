<?php
$dictionary['Contact']['fields']['gender_c']=array(
            'name'=> 'gender_c',
            'type' => 'enum',
            'options'=> 'gender_list_strings',
            'isnull' => 0,
            'vname' => 'LBL_GENDER',
);
