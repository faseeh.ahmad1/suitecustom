<?php
$dictionary['Contact']['fields']['section_student']=array(
    'name' => 'section_student',
    'type' => 'link',
    'relationship' => 'section_student',
    'module' => 'reg_Section',
    'bean_name' => 'reg_Section',
    'source' => 'non-db',
    'vname' => 'LBL_SECTION'
);
