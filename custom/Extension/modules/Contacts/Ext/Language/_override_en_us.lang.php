<?php
// created: 2022-08-02 17:33:12
$mod_strings['LNK_NEW_CONTACT'] = 'Create Student';
$mod_strings['LNK_CONTACT_LIST'] = 'View Students';
$mod_strings['LNK_IMPORT_VCARD'] = 'Create Student From vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Import Students';
$mod_strings['LBL_SAVE_CONTACT'] = 'Save Student';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Student List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Student Search';
$mod_strings['LBL_REPORTS_TO'] = 'Reports To:';
$mod_strings['LBL_DIRECT_REPORTS'] = 'Direct Reports';
$mod_strings['LBL_DIRECT_REPORTS_SUBPANEL_TITLE'] = 'Direct Reports';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Students';
$mod_strings['LBL_MODULE_NAME'] = 'Students';
$mod_strings['LBL_ROLLNO'] = 'Roll No';
$mod_strings['LBL_GENDER'] = 'Gender';
$mod_strings['LBL_SECTION'] = 'Section';
$mod_strings['LBL_SUB_PANEL_SECTIONS'] = 'Sections';
$mod_strings['LBL_NO_OF_SECTIONS'] = 'No. of Sections';





