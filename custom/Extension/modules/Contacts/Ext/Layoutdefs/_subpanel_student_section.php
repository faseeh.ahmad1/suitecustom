<?php
$layout_defs["Contacts"]["subpanel_setup"]['section_student'] = array(
    'order' => 100,
    'module' => 'reg_Section',
    'subpanel_name' => 'default',
    'sort_order' => 'asc',
    'sort_by' => 'id',
    'title_key' => 'LBL_SUB_PANEL_SECTIONS',
    'get_subpanel_data' => 'section_student',
    'top_buttons' => 
    array (
      0 => 
      array (
        'widget_class' => 'SubPanelTopButtonQuickCreate',
      ),
      1 => 
      array (
        'widget_class' => 'SubPanelTopSelectButton',
        'mode' => 'MultiSelect',
      ),
    )
);