<?php
$module_name = 'reg_Course';
$viewdefs [$module_name] =
    array(
        'EditView' =>
            array(
                'templateMeta' =>
                    array(
                        'maxColumns' => '2',
                        'widths' =>
                            array(
                                0 =>
                                    array(
                                        'label' => '10',
                                        'field' => '30',
                                    ),
                                1 =>
                                    array(
                                        'label' => '10',
                                        'field' => '30',
                                    ),
                            ),
                        'useTabs' => false,
                        'tabDefs' =>
                            array(
                                'DEFAULT' =>
                                    array(
                                        'newTab' => false,
                                        'panelDefault' => 'expanded',
                                    ),
                            ),
                        'includes' => array(
                            array(
                                'file' => 'modules/reg_Course/js/courseValidation.js',
                            ),
                        )
                    ),
                'panels' =>
                    array(
                        'default' =>
                            array(
                                0 =>
                                    array(
                                        0 => 'name',
                                    ),
                                1 =>
                                    array(
                                        0 => 'description',
                                    ),
                                2 =>
                                    array(
                                        0 =>
                                            array(
                                                'name' => 'course_code',
                                                'label' => 'LBL_COURSECODE',
                                            ),
                                        1 => '',
                                    ),
                            ),
                    ),
            ),
    );
?>
