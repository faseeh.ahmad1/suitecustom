<?php 
 //WARNING: The contents of this file are auto-generated


$dictionary['Contact']['fields']['section_student']=array(
    'name' => 'section_student',
    'type' => 'link',
    'relationship' => 'section_student',
    'module' => 'reg_Section',
    'bean_name' => 'reg_Section',
    'source' => 'non-db',
    'vname' => 'LBL_SECTION'
);


$dictionary['Contact']['fields']['gender_c']=array(
            'name'=> 'gender_c',
            'type' => 'enum',
            'options'=> 'gender_list_strings',
            'isnull' => 0,
            'vname' => 'LBL_GENDER',
);


$dictionary['Contact']['fields']['sections_c'] = array(
    'name' => 'sections_c',
    'vname' => 'LBL_NO_OF_SECTIONS',
    'type' => 'text',
    'isnull' => 1,
    'source' => 'non-db',
    'studio' => 1,
);



$dictionary['Contact']['fields']['roll_no_c']=array(
            'name'=> 'roll_no_c',
            'type'=> 'varchar',
            'isnull'=> 0,
            'vname'=> 'LBL_ROLLNO'
);


 // created: 2022-08-02 17:30:58
$dictionary['Contact']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2022-08-02 17:30:58
$dictionary['Contact']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2022-08-02 17:30:58
$dictionary['Contact']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2022-08-02 17:30:58
$dictionary['Contact']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 
?>