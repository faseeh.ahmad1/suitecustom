<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once 'modules/Contacts/views/view.detail.php';

class CustomContactsViewDetail extends ContactsViewDetail
{
    public function display()
    {
        $query = "SELECT COUNT(*) as count FROM `section_student` WHERE student_id='" . $this->bean->id . "'";
        $result = $GLOBALS["db"]->query($query);
        $no_of_sections = $GLOBALS["db"]->fetchByAssoc($result)['count'];
        $this->bean->sections_c = $no_of_sections;
        parent::display();

        echo $no_of_sections > 0
            ? "<h3>THE STUDENT IS ENROLLED IN $no_of_sections SECTIONS</h3>"
            : "<h3>Student is not enrolled in any section</h3>";
    }
}