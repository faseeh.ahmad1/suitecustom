<?php
$module_name = 'reg_Teacher';
$listViewDefs [$module_name] = 
array (
  'EMAIL' => 
  array (
    'type' => 'varchar(255)',
    'label' => 'Email',
    'width' => '10%',
    'default' => true,
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'PHONE' => 
  array (
    'type' => 'varchar(255)',
    'label' => 'Phone',
    'width' => '10%',
    'default' => true,
  ),
  'AGE' => 
  array (
    'type' => 'int',
    'label' => 'Age',
    'width' => '10%',
    'default' => true,
  ),
  'DOB' => 
  array (
    'type' => 'date',
    'label' => 'LBL_DATE_OF_BIRTH',
    'width' => '10%',
    'default' => true,
  ),
  'GENDER' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_GENDER',
    'width' => '10%',
    'default' => true,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
