<?php
$module_name = 'reg_Section';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'TEACHER_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_TEACHERNAME',
    'id' => 'TEACHER_ID',
    'width' => '10%',
    'default' => true,
  ),
  'COURSE_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_COUSE_NAME',
    'id' => 'COURSE_ID',
    'width' => '10%',
    'default' => true,
  ),
);
;
?>
