<?php
$module_name = 'reg_Section';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'section_label',
            'label' => 'LBL_SECTION_LABEL',
          ),
        ),
        1 => 
        array (
          0 => 'description',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'teacher_name',
            'label' => 'LBL_TEACHERNAME',
          ),
          1 => 
          array (
            'name' => 'course_name',
            'label' => 'LBL_COUSE_NAME',
          ),
        ),
      ),
    ),
  ),
);
;
?>
