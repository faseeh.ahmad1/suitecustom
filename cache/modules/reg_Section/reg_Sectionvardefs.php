<?php 
 $GLOBALS["dictionary"]["reg_Section"]=array (
  'table' => 'reg_section',
  'audited' => true,
  'inline_edit' => true,
  'duplicate_merge' => true,
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'vname' => 'LBL_ID',
      'type' => 'id',
      'required' => true,
      'reportable' => true,
      'comment' => 'Unique identifier',
      'inline_edit' => false,
    ),
    'name' => 
    array (
      'name' => 'name',
      'vname' => 'LBL_NAME',
      'type' => 'name',
      'link' => true,
      'dbType' => 'varchar',
      'len' => 255,
      'unified_search' => true,
      'full_text_search' => 
      array (
        'boost' => 3,
      ),
      'required' => true,
      'importable' => 'required',
      'duplicate_merge' => 'enabled',
      'merge_filter' => 'selected',
    ),
    'date_entered' => 
    array (
      'name' => 'date_entered',
      'vname' => 'LBL_DATE_ENTERED',
      'type' => 'datetime',
      'group' => 'created_by_name',
      'comment' => 'Date record created',
      'enable_range_search' => true,
      'options' => 'date_range_search_dom',
      'inline_edit' => false,
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'vname' => 'LBL_DATE_MODIFIED',
      'type' => 'datetime',
      'group' => 'modified_by_name',
      'comment' => 'Date record last modified',
      'enable_range_search' => true,
      'options' => 'date_range_search_dom',
      'inline_edit' => false,
    ),
    'modified_user_id' => 
    array (
      'name' => 'modified_user_id',
      'rname' => 'user_name',
      'id_name' => 'modified_user_id',
      'vname' => 'LBL_MODIFIED',
      'type' => 'assigned_user_name',
      'table' => 'users',
      'isnull' => 'false',
      'group' => 'modified_by_name',
      'dbType' => 'id',
      'reportable' => true,
      'comment' => 'User who last modified record',
      'massupdate' => false,
      'inline_edit' => false,
    ),
    'modified_by_name' => 
    array (
      'name' => 'modified_by_name',
      'vname' => 'LBL_MODIFIED_NAME',
      'type' => 'relate',
      'reportable' => false,
      'source' => 'non-db',
      'rname' => 'user_name',
      'table' => 'users',
      'id_name' => 'modified_user_id',
      'module' => 'Users',
      'link' => 'modified_user_link',
      'duplicate_merge' => 'disabled',
      'massupdate' => false,
      'inline_edit' => false,
    ),
    'created_by' => 
    array (
      'name' => 'created_by',
      'rname' => 'user_name',
      'id_name' => 'modified_user_id',
      'vname' => 'LBL_CREATED',
      'type' => 'assigned_user_name',
      'table' => 'users',
      'isnull' => 'false',
      'dbType' => 'id',
      'group' => 'created_by_name',
      'comment' => 'User who created record',
      'massupdate' => false,
      'inline_edit' => false,
    ),
    'created_by_name' => 
    array (
      'name' => 'created_by_name',
      'vname' => 'LBL_CREATED',
      'type' => 'relate',
      'reportable' => false,
      'link' => 'created_by_link',
      'rname' => 'user_name',
      'source' => 'non-db',
      'table' => 'users',
      'id_name' => 'created_by',
      'module' => 'Users',
      'duplicate_merge' => 'disabled',
      'importable' => 'false',
      'massupdate' => false,
      'inline_edit' => false,
    ),
    'description' => 
    array (
      'name' => 'description',
      'vname' => 'LBL_DESCRIPTION',
      'type' => 'text',
      'comment' => 'Full text of the note',
      'rows' => 6,
      'cols' => 80,
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'vname' => 'LBL_DELETED',
      'type' => 'bool',
      'default' => '0',
      'reportable' => false,
      'comment' => 'Record deletion indicator',
    ),
    'created_by_link' => 
    array (
      'name' => 'created_by_link',
      'type' => 'link',
      'relationship' => 'reg_section_created_by',
      'vname' => 'LBL_CREATED_USER',
      'link_type' => 'one',
      'module' => 'Users',
      'bean_name' => 'User',
      'source' => 'non-db',
    ),
    'modified_user_link' => 
    array (
      'name' => 'modified_user_link',
      'type' => 'link',
      'relationship' => 'reg_section_modified_user',
      'vname' => 'LBL_MODIFIED_USER',
      'link_type' => 'one',
      'module' => 'Users',
      'bean_name' => 'User',
      'source' => 'non-db',
    ),
    'assigned_user_id' => 
    array (
      'name' => 'assigned_user_id',
      'rname' => 'user_name',
      'id_name' => 'assigned_user_id',
      'vname' => 'LBL_ASSIGNED_TO_ID',
      'group' => 'assigned_user_name',
      'type' => 'relate',
      'table' => 'users',
      'module' => 'Users',
      'reportable' => true,
      'isnull' => 'false',
      'dbType' => 'id',
      'audited' => true,
      'comment' => 'User ID assigned to record',
      'duplicate_merge' => 'disabled',
    ),
    'assigned_user_name' => 
    array (
      'name' => 'assigned_user_name',
      'link' => 'assigned_user_link',
      'vname' => 'LBL_ASSIGNED_TO_NAME',
      'rname' => 'user_name',
      'type' => 'relate',
      'reportable' => false,
      'source' => 'non-db',
      'table' => 'users',
      'id_name' => 'assigned_user_id',
      'module' => 'Users',
      'duplicate_merge' => 'disabled',
    ),
    'assigned_user_link' => 
    array (
      'name' => 'assigned_user_link',
      'type' => 'link',
      'relationship' => 'reg_section_assigned_user',
      'vname' => 'LBL_ASSIGNED_TO_USER',
      'link_type' => 'one',
      'module' => 'Users',
      'bean_name' => 'User',
      'source' => 'non-db',
      'duplicate_merge' => 'enabled',
      'rname' => 'user_name',
      'id_name' => 'assigned_user_id',
      'table' => 'users',
    ),
    'SecurityGroups' => 
    array (
      'name' => 'SecurityGroups',
      'type' => 'link',
      'relationship' => 'securitygroups_reg_section',
      'module' => 'SecurityGroups',
      'bean_name' => 'SecurityGroup',
      'source' => 'non-db',
      'vname' => 'LBL_SECURITYGROUPS',
    ),
    'section_label' => 
    array (
      'name' => 'section_label',
      'type' => 'varchar',
      'isnull' => 0,
      'vname' => 'LBL_SECTION_LABEL',
    ),
    'teacher_id' => 
    array (
      'name' => 'teacher_id',
      'type' => 'id',
      'isnull' => 1,
      'vname' => 'LBL_TEACHERID',
    ),
    'teacher_mail' => 
    array (
      'name' => 'teacher_mail',
      'vname' => 'LBL_TEACHERMAIL',
      'type' => 'varchar',
      'isnull' => 1,
      'source' => 'non-db',
      'studio' => 1,
    ),
    'course_code' => 
    array (
      'name' => 'course_code',
      'vname' => 'LBL_COURSE_CODE',
      'type' => 'varchar',
      'isnull' => 1,
      'source' => 'non-db',
      'studio' => 1,
    ),
    'teacher_name' => 
    array (
      'name' => 'teacher_name',
      'vname' => 'LBL_TEACHERNAME',
      'type' => 'relate',
      'rname' => 'name',
      'link' => 'teacher_sections',
      'table' => 'reg_teacher',
      'module' => 'reg_Teacher',
      'id_name' => 'teacher_id',
      'isnull' => 1,
      'source' => 'non-db',
    ),
    'teacher_sections' => 
    array (
      'name' => 'teacher_sections',
      'type' => 'link',
      'relationship' => 'teacher_sections',
      'module' => 'reg_Teacher',
      'bean_name' => 'reg_Teacher',
      'source' => 'non-db',
      'vname' => 'LBL_TEACHERS',
    ),
    'course_id' => 
    array (
      'name' => 'course_id',
      'type' => 'id',
      'isnull' => 1,
      'vname' => 'LBL_COURSEID',
    ),
    'course_name' => 
    array (
      'name' => 'course_name',
      'vname' => 'LBL_COUSE_NAME',
      'type' => 'relate',
      'rname' => 'name',
      'link' => 'section_courses',
      'table' => 'reg_course',
      'module' => 'reg_Course',
      'id_name' => 'course_id',
      'isnull' => 1,
      'source' => 'non-db',
    ),
    'section_courses' => 
    array (
      'name' => 'section_courses',
      'type' => 'link',
      'relationship' => 'section_courses',
      'module' => 'reg_Course',
      'bean_name' => 'reg_Course',
      'source' => 'non-db',
      'vname' => 'LBL_COURSES',
    ),
    'section_student' => 
    array (
      'name' => 'section_student',
      'type' => 'link',
      'relationship' => 'section_student',
      'module' => 'Contacts',
      'bean_name' => 'Contacts',
      'source' => 'non-db',
      'vname' => 'LBL_CONTACTS',
    ),
  ),
  'relationships' => 
  array (
    'reg_section_modified_user' => 
    array (
      'lhs_module' => 'Users',
      'lhs_table' => 'users',
      'lhs_key' => 'id',
      'rhs_module' => 'reg_Section',
      'rhs_table' => 'reg_section',
      'rhs_key' => 'modified_user_id',
      'relationship_type' => 'one-to-many',
    ),
    'reg_section_created_by' => 
    array (
      'lhs_module' => 'Users',
      'lhs_table' => 'users',
      'lhs_key' => 'id',
      'rhs_module' => 'reg_Section',
      'rhs_table' => 'reg_section',
      'rhs_key' => 'created_by',
      'relationship_type' => 'one-to-many',
    ),
    'reg_section_assigned_user' => 
    array (
      'lhs_module' => 'Users',
      'lhs_table' => 'users',
      'lhs_key' => 'id',
      'rhs_module' => 'reg_Section',
      'rhs_table' => 'reg_section',
      'rhs_key' => 'assigned_user_id',
      'relationship_type' => 'one-to-many',
    ),
    'securitygroups_reg_section' => 
    array (
      'lhs_module' => 'SecurityGroups',
      'lhs_table' => 'securitygroups',
      'lhs_key' => 'id',
      'rhs_module' => 'reg_Section',
      'rhs_table' => 'reg_section',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'securitygroups_records',
      'join_key_lhs' => 'securitygroup_id',
      'join_key_rhs' => 'record_id',
      'relationship_role_column' => 'module',
      'relationship_role_column_value' => 'reg_Section',
    ),
    'teacher_sections' => 
    array (
      'lhs_module' => 'reg_Teacher',
      'lhs_table' => 'reg_teacher',
      'lhs_key' => 'id',
      'rhs_module' => 'reg_Section',
      'rhs_table' => 'reg_section',
      'rhs_key' => 'teacher_id',
      'relationship_type' => 'one-to-many',
    ),
    'section_courses' => 
    array (
      'lhs_module' => 'reg_Course',
      'lhs_table' => 'reg_course',
      'lhs_key' => 'id',
      'rhs_module' => 'reg_Section',
      'rhs_table' => 'reg_section',
      'rhs_key' => 'course_id',
      'relationship_type' => 'one-to-many',
    ),
  ),
  'optimistic_locking' => true,
  'unified_search' => true,
  'indices' => 
  array (
    'id' => 
    array (
      'name' => 'reg_sectionpk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
  ),
  'templates' => 
  array (
    'security_groups' => 'security_groups',
    'assignable' => 'assignable',
    'basic' => 'basic',
  ),
  'custom_fields' => false,
);