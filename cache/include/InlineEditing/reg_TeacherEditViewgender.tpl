

<select name="{$fields.gender.name}"
        id="{$fields.gender.name}"
        title=''  tabindex="1"          
        >

    {if isset($fields.gender.value) && $fields.gender.value != ''}
        {html_options options=$fields.gender.options selected=$fields.gender.value}
    {else}
        {html_options options=$fields.gender.options selected=$fields.gender.default}
    {/if}
</select>