<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

$dictionary['reg_Section'] = array(
    'table' => 'reg_section',
    'audited' => true,
    'inline_edit' => true,
    'duplicate_merge' => true,
    'fields' => array (
        'section_label' => array(
            'name' => 'section_label',
            'type' => 'varchar',
            'isnull' => 0,
            'vname' => 'LBL_SECTION_LABEL'
        ),
        'teacher_id' => array(
            'name' => 'teacher_id',
            'type' => 'id',
            'isnull' => 1,
            'vname' => 'LBL_TEACHERID'
        ),
        'teacher_mail' => array(
            'name' => 'teacher_mail',
            'vname' => 'LBL_TEACHERMAIL',
            'type' => 'varchar',
            'isnull' => 1,
            'source' => 'non-db',
            'studio' => 1
        ),
        'course_code' => array(
            'name' => 'course_code',
            'vname' => 'LBL_COURSE_CODE',
            'type' => 'varchar',
            'isnull' => 1,
            'source' => 'non-db',
            'studio' => 1
        ),
        'teacher_name' => array(
            'name' => 'teacher_name',
            'vname' => 'LBL_TEACHERNAME',
            'type' => 'relate',
            'rname' => 'name',
            'link' => 'teacher_sections',
            'table' => 'reg_teacher',
            'module' => 'reg_Teacher',
            'id_name' => 'teacher_id',
            'isnull' => 1,
            'source' => 'non-db'
        ),
        'teacher_sections' => array(
            'name' => 'teacher_sections',
            'type' => 'link',
            'relationship' => 'teacher_sections',
            'module' => 'reg_Teacher',
            'bean_name' => 'reg_Teacher',
            'source' => 'non-db',
            'vname' => 'LBL_TEACHERS',
        ),
//-----------------------------------------------------------------------------------
        'course_id' => array(
            'name' => 'course_id',
            'type' => 'id',
            'isnull' => 1,
            'vname' => 'LBL_COURSEID'
        ),
        'course_name' => array(
            'name' => 'course_name',
            'vname' => 'LBL_COUSE_NAME',
            'type' => 'relate',
            'rname' => 'name',
            'link' => 'section_courses',
            'table' => 'reg_course',
            'module' => 'reg_Course',
            'id_name' => 'course_id',
            'isnull' => 1,
            'source' => 'non-db'
        ),
        'section_courses' => array(
            'name' => 'section_courses',
            'type' => 'link',
            'relationship' => 'section_courses',
            'module' => 'reg_Course',
            'bean_name' => 'reg_Course',
            'source' => 'non-db',
            'vname' => 'LBL_COURSES',
        ),
//-----------------------------------------------------------------------------------
    'section_student' => array(
        'name' => 'section_student',
        'type' => 'link',
        'relationship' => 'section_student',
        'module' => 'Contacts',
        'bean_name' => 'Contacts',
        'source' => 'non-db',
        'vname' => 'LBL_CONTACTS',
    )
),
'relationships' => array (
    'teacher_sections' => array(
        'lhs_module' => 'reg_Teacher',
        'lhs_table' => 'reg_teacher',
        'lhs_key' => 'id',
        'rhs_module' => 'reg_Section',
        'rhs_table' => 'reg_section',
        'rhs_key' => 'teacher_id',
        'relationship_type' => 'one-to-many'
    ),
    'section_courses' => array(
        'lhs_module' => 'reg_Course',
        'lhs_table' => 'reg_course',
        'lhs_key' => 'id',
        'rhs_module' => 'reg_Section',
        'rhs_table' => 'reg_section',
        'rhs_key' => 'course_id',
        'relationship_type' => 'one-to-many'
    )
),
    'optimistic_locking' => true,
    'unified_search' => true,
);
if (!class_exists('VardefManager')) {
        require_once('include/SugarObjects/VardefManager.php');
}
VardefManager::createVardef('reg_Section', 'reg_Section', array('basic','assignable','security_groups'));