<?php

require_once 'include/MVC/View/views/view.detail.php';

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class reg_SectionViewDetail extends ViewDetail
{

    public function display(){
        $section  = $this->bean;
        $course = BeanFactory::getBean('reg_Course', $section->course_id);
        $teacher = BeanFactory::getBean('reg_Teacher', $section->teacher_id);
        $section->teacher_mail = $teacher->email;
        $section->course_code = $course->course_code;

        parent::display();

    }
}