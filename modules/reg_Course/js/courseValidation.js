$(document).ready(function () {
    $(".edit-view-row").prepend(`<div class="col-xs-12 col-sm-12 edit-view-row-item" data-field="test">
        <div class="col-xs-12 col-sm-2 label" data-label="LBL_TEST">TEST:<span class="required">*</span></div>
        <div class="col-xs-12 col-sm-8 edit-view-field " type="name" field="name" colspan="3">
        <input type="text" name="test" id="test" size="30" maxlength="255"  title="">
        </div>
        </div>`
    );

    if ($("#name").val().length > 0) {
        addToValidate('EditView', 'course_code', 'varchar', true, `${$('#course_code').parent().prev().html()}`);
        addToValidate('EditView', 'test', 'varchar', true, `${$('#test').parent().prev().html()}`);

    }


    $("#course_code").change(function () {
        const course_code = $(`#course_code`).val();
        console.log(course_code);
        $.ajax({
            type: "POST", url: "index.php?module=reg_Course&action=ValidateCourse", data: {
                course_code, id: $('input[type=hidden][name=record]').val()
            }, success: function (response) {
                data = JSON.parse(response);
                const {duplicate} = data;
                if (duplicate) {
                    $('#course_code').val("");
                    alert("COURSE WITH SAME CODE ALREADY EXISTS");
                }
            }, err: function () {
                // console.log(err);
            }
        });
    });
});



