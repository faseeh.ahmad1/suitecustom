<?php
$module_name = 'reg_Teacher';
$layout_defs[$module_name]['subpanel_setup']['securitygroups'] = array(
    'top_buttons' => array(array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'SecurityGroups', 'mode' => 'MultiSelect')),
    'order' => 900,
    'sort_by' => 'name',
    'sort_order' => 'asc',
    'module' => 'SecurityGroups',
    'refresh_page' => 1,
    'subpanel_name' => 'default',
    'get_subpanel_data' => 'SecurityGroups',
    'add_subpanel_data' => 'securitygroup_id',
    'title_key' => 'LBL_SECURITYGROUPS_SUBPANEL_TITLE',
);

$layout_defs[$module_name]["subpanel_setup"]['teacher_sections'] = array (
    'order' => 100,
    'module' => 'reg_Section',
    'subpanel_name' => 'for_teachers',
    'sort_order' => 'asc',
    'sort_by' => 'id',
    'title_key' => 'LBL_SUB_PANEL_SECTIONS',
    'get_subpanel_data' => 'teacher_sections',
    'top_buttons' => 
    array (
      0 => 
      array (
        'widget_class' => 'SubPanelTopButtonQuickCreate',
      ),
      1 => 
      array (
        'widget_class' => 'SubPanelTopSelectButton',
        'mode' => 'MultiSelect',
      ),
    ),
  );