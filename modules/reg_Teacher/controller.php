<?php
//controller to test email sending
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/reg_Teacher/BirthdayWishes.php');

class reg_TeacherController extends SugarController
{

    public function action_Test()
    {
        echo birthdayWishes::sendBirthdayWishes();
        die ('');
    }


    function action_Login(){
        $result = $this->restRequest('login', array(
            'user_auth' => array(
                'user_name' => 'admin',
                'password' => md5('123'),
            ),
            'application_name' => '',
            'name_value_list' => array()
        ));
        echo json_encode($result);
        global $sessId;
        $sessId = $result['id'];
        echo $sessId;
        die();
    }

    function action_Get(){
        $result2 = $this->restRequest('login', array(
            'user_auth' => array(
                'user_name' => 'admin',
                'password' => md5('123'),
            ),
            'application_name' => '',
            'name_value_list' => array()
        ));
        $sessId = $result2['id'];

        $result = $this->restRequest('get_entry_list', array(
            //Session id - retrieved from login call
            'session' => $sessId,
            //Module to get_entry_list for
            'module_name' => 'reg_Section',
            //Filter query - Added to the SQL where clause,
            'query' => "",
            //Order by - unused
            'order_by' => '',
            //Start with the first record
            'offset' => 0,
            //Return the id and name fields
            'select_fields' => array('id', 'name',),
            //Link to the "contacts" relationship and retrieve the
            //First and last names.
            'link_name_to_fields_array' => array(
                array(
                    'name' => 'section_student',
                    'value' => array(
                        'first_name',
                        'last_name',
                        'name'
                    ),
                ),
            ),
            //Show 10 max results
            'max_results' => 10,
            //Do not show deleted
            'deleted' => 0,
        ));
        echo json_encode($result);
        die();
    }

    function restRequest($method, $arguments)
    {
        $url = "http://localhost/suiteCustom/service/v4_1/rest.php";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $post = array(
            "method" => $method,
            "input_type" => "JSON",
            "response_type" => "JSON",
            "rest_data" => json_encode($arguments),
        );

        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);

        $result = curl_exec($curl);
        curl_close($curl);
        return json_decode($result, 1);
    }

}