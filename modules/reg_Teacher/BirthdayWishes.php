<?php
include_once('include/SugarPHPMailer.php');
include_once('include/utils/db_utils.php'); // for from_html function

class birthdayWishes
{
    public static function sendBirthdayWishes()
    {
        global $db;
        $query = "SELECT * FROM reg_teacher WHERE
                    MONTH(dob) = MONTH(CURRENT_DATE()) 
                    AND DAY(dob) = DAY(CURRENT_DATE())";

        $result = $db->query($query);
        while ($teacher = $db->fetchByAssoc($result)) {
            $mail = new SugarPHPMailer();
            $mail->From = "no-reply@example.com";
            $mail->FromName = "Faseeh Ahmad";
            $mail->ClearAllRecipients();
            $mail->ClearReplyTos();
            $mail->AddAddress($teacher['email'], $teacher['name']);
            $mail->Subject = "Birthday Wish";
            $mail->Body_html = from_html("<h1>Happy Birthday Dear " . $teacher['name'] . "</h1>");
            $mail->Body = wordwrap("<h1>Happy Birthday Dear " . $teacher['name'] . "</h1>", 900);
            $mail->isHTML(true); // set to true if content has html tags
            //preparing email for sending
            $mail->prepForOutbound();
            $mail->setMailerForSystem();

            if (!$mail->Send()) {
                $GLOBALS['log']->fatal("ERROR: Mail sending failed!");
            }
        }
        return true;
    }
}